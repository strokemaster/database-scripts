# Create the table for Users

CREATE TABLE User
(
ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
Email varchar(50) UNIQUE,
# Password is a hashed value
Password varchar (50),
# Clubs is a comma seperated list of the clubs in the bag - "Driver, 3 Wood, 5 Hybrid, 6 Iron..."
Clubs varchar(250)
);

# List of clubs that can be called in the Set Bag Page
CREATE TABLE Club
(
ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
clubName varchar(50) UNIQUE
);


# Create the Table for Rounds

CREATE TABLE Round
(
# SUPER IMPORTANT!!! Unique format - User ID + Timestamp
ID varchar(100) NOT NULL PRIMARY KEY,
UserID varchar(100) NOT NULL,
CourseName varchar(100) NOT NULL,
# Comma seperated list of the par values for all holes played "3,4,5,4,5"
Par varchar(100) NOT NULL,
# Comma seperated list of what the user shot on each hole i.e. "4,5,8,9,6,3,10"
Score varchar (100) NOT NULL,
# 72,80,100 ... everything added up for nice and easy stats calculations
TotalPar int NOT NULL,
TotalScore int NOT NULL,
# The hole the user started on
startHole int NOT NULL,
# 9 or 18
TotalHoles int NOT NULL,
Date DATE NOT NULL,
FOREIGN KEY (UserID) REFERENCES User (Email) ON UPDATE NO ACTION ON DELETE CASCADE
);

# Create the Table for Shots

# Penalties startPoint=endPoint,Distance=null,Club=penalty
Create TABLE Shot
(
ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
RoundID varchar(100) NOT NULL,
CourseName varchar(100) NOT NULL,
UserID varchar(100) NOT NULL,
# Hole corresponds to the hole this shot was hit on Example: the 4th hole = 4
Hole int NOT NULL,
# Is this the 1st shot you are hitting, or is it your 4th?
Stroke int NOT NULL,
# The GPS Coordinates which represent the points. Can be used to give users a visual representation of their round ... eventually
startPoint varchar (40) NOT NULL,
endPoint varchar (40) NOT NULL,
# The distance to the hole before the shot was taken.  Used for stats calculations.  
startDistance int NOT NULL,
# The distance to the hole after the shot was taken.  Used for stats calculations.  
endDistance int NOT NULL,
# Total distance the ball traveled. Calculate it on insertion (API Layer) to reduce processing later
# equivalent to startDistance minus endDistance
Yardage int NOT NULL,
# Rough, Green, Fairway, etc. - figure this out on the API layer on insertion
Terrain varchar (20) NOT NULL,
Club varchar (40) NOT NULL,
FOREIGN KEY (RoundID) REFERENCES Round (ID) ON UPDATE NO ACTION ON DELETE CASCADE,
FOREIGN KEY (UserID) REFERENCES User (Email) ON UPDATE NO ACTION ON DELETE CASCADE
);

# Not really used for the alpha except for when I insert test rounds.  Ignore it.  
# Create the Table for Courses

CREATE TABLE Course
(
ID int NOT NULL PRIMARY KEY,
CourseName varchar (100),
# Par is a comma seperated list for all holes in the course 
Par varchar (100) NOT NULL,
# Yardage is the distance from the tee to green on each hole 
Yardage varchar (100)
);


#######################################################
# COMMENTED OUT because we are skipping it for the alpha
# Create the table for User Profiles

#CREATE TABLE UserProfile
#(
#UserID int NOT NULL PRIMARY KEY,
#LastName varchar(50),
#FirstName varchar(50),
#Address varchar(255),
#City varchar(50),
#Clubs varchar(255),
#Gender varchar(10),
#FOREIGN KEY (UserID) REFERENCES User (ID) ON UPDATE NO ACTION ON DELETE CASCADE
#);
