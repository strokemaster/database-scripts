# Original UserID
DELETE FROM Round WHERE UserID IN (1, 2, 3, 4);
DELETE FROM Shot WHERE UserID IN (1, 2, 3, 4);

# Updated UserID
DELETE FROM Round WHERE UserID IN ("bill.clinton@whitehouse.gov", "luke.skywalker@jeditemple.org", "han.solo@milleniumfalcon.com", "robert.downey@jr.com");
DELETE FROM Shot WHERE UserID IN ("bill.clinton@whitehouse.gov", "luke.skywalker@jeditemple.org", "han.solo@milleniumfalcon.com", "robert.downey@jr.com");
