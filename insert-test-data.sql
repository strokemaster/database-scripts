# Insert data for test users - add more through the actual app!
INSERT INTO `User` (`Email`, `Password`) VALUES ("bill.clinton@whitehouse.gov", "pleadThe5th");
INSERT INTO `User` (`Email`, `Password`) VALUES ("luke.skywalker@jeditemple.org", "thereisNoTry");
INSERT INTO `User` (`Email`, `Password`) VALUES ("han.solo@milleniumfalcon.com", "IShotFirst");
INSERT INTO `User` (`Email`, `Password`) VALUES ("robert.downey@jr.com", "ironManGetIt?");

# Insert data for test courses into Course

INSERT INTO `Course` (`ID`,`CourseName`,`Par`,`Yardage`) VALUES (123456,"James Baird State Park","3,4,5,4,3,4,5,3,4,5,3,4,5,3,4,5,4,3","325,425,525,445,320,440,450,330,470,555,333,444,565,321,442,542,442,333");

INSERT INTO `Course` (`ID`,`CourseName`,`Par`,`Yardage`) VALUES (112355,"Augusta National","4,5,4,5,4,5,5,5,4,3,3,4,5,5,4,4,5,4","465,555,485,595,420,540,556,550,430,355,320,484,555,521,482,452,522,483");

INSERT INTO `Course` (`ID`,`CourseName`,`Par`,`Yardage`) VALUES (423786,"Fuck You Course","5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5","555,525,535,585,560,540,590,533,573,555,543,545,568,521,542,552,522,535");

INSERT INTO `Course` (`ID`,`CourseName`,`Par`,`Yardage`) VALUES (999999,"Infinity Links","3,4,5,4,5,4,4,5,4,4,4,3,3,3,4,4,4,4","313,445,555,425,420,540,440,430,525,455,433,444,365,319,482,442,422,433");

INSERT INTO `Course` (`ID`,`CourseName`,`Par`,`Yardage`) VALUES (987654,"Sawgrass", "3,4,4,4,3,5,5,5,4,4,3,3,3,4,5,5,4,5","380,445,425,465,319,540,550,530,480,455,323,344,365,421,542,542,444,533");

# End course data

# Insert Club Data
INSERT INTO `Club` (`clubName`) VALUES ("Driver");
INSERT INTO `Club` (`clubName`) VALUES ("3 Wood");
INSERT INTO `Club` (`clubName`) VALUES ("4 Wood");
INSERT INTO `Club` (`clubName`) VALUES ("5 Wood");
INSERT INTO `Club` (`clubName`) VALUES ("2 Hybrid");
INSERT INTO `Club` (`clubName`) VALUES ("3 Hybrid");
INSERT INTO `Club` (`clubName`) VALUES ("4 Hybrid");
INSERT INTO `Club` (`clubName`) VALUES ("5 Hybrid");
INSERT INTO `Club` (`clubName`) VALUES ("6 Hybrid");
INSERT INTO `Club` (`clubName`) VALUES ("7 Hybrid");
INSERT INTO `Club` (`clubName`) VALUES ("1 Iron");
INSERT INTO `Club` (`clubName`) VALUES ("2 Iron");
INSERT INTO `Club` (`clubName`) VALUES ("3 Iron");
INSERT INTO `Club` (`clubName`) VALUES ("4 Iron");
INSERT INTO `Club` (`clubName`) VALUES ("5 Iron");
INSERT INTO `Club` (`clubName`) VALUES ("6 Iron");
INSERT INTO `Club` (`clubName`) VALUES ("7 Iron");
INSERT INTO `Club` (`clubName`) VALUES ("8 Iron");
INSERT INTO `Club` (`clubName`) VALUES ("9 Iron");
INSERT INTO `Club` (`clubName`) VALUES ("Pitching Wedge");
INSERT INTO `Club` (`clubName`) VALUES ("Sand Wedge");
INSERT INTO `Club` (`clubName`) VALUES ("Loft Wedge");
INSERT INTO `Club` (`clubName`) VALUES ("Flop Wedge");

# Not used in the Alpha
# Insert test data into UserProfile
#INSERT INTO `UserProfile` (`UserID`,`LastName`,`FirstName`,`Address`, `City`, `Gender`) VALUES (1, "Baxter", "Sean", "3036 16th Ave W 304", "Seattle", "Male");

#INSERT INTO `UserProfile` (`UserID`,`LastName`,`FirstName`,`Address`, `City`, `Gender`) VALUES (3, "Morano", "Greg", "???", "Houston", "Male");

#INSERT INTO `UserProfile` (`UserID`,`LastName`,`FirstName`,`Address`, `City`, `Gender`) VALUES (4, "User", "Test1", "200 Main St", "Smalltown USA", "Male");

# End User Profile data
